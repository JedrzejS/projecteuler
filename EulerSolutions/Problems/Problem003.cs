﻿using EulerSolutions.Common;

namespace EulerSolutions.Problems
{
    internal class Problem003 : IProblem
    {
        public string Key => "Problem003 - Largest prime factor";
        public string Result => FindLargestPrimeFactor().ToString();

        private long FindLargestPrimeFactor(long input = 600851475143)
        {
            long maxPrimeFactor = 0;
            for (long i = 2; i <= input / 2; i++)
            {
                if (input % i == 0 && IsPrime(i) && i > maxPrimeFactor)
                    maxPrimeFactor = i;
            }
            return maxPrimeFactor;
        }
        private bool IsPrime(long x)
        {
            for (long i = 2; i <= x / 2; i++)
                if (x % i == 0)
                    return false;
            return true;
        }
    }
}
