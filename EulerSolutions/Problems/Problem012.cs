﻿using EulerSolutions.Common;
using System.Collections.Generic;

namespace EulerSolutions.Problems
{
    internal class Problem012 : IProblem
    {
        public string Key => "Problem012 - Highly divisible triangular number";
        public string Result => FindTriangleNumber().ToString();

        private int FindTriangleNumber()
        {
            var triangleNumbers = GenerateTriangleNumbers(10);
            int devNo = 0;
            foreach (var trNo in triangleNumbers)
            {

            }
            return 0;
        }

        private List<int> GenerateTriangleNumbers(int no)
        {
            var retList = new List<int>() { 1 };
            for (int i = 2; i <= no; i++)
            {
                retList.Add(retList[retList.Count - 1] + i);
            }
            return retList;
        }
    }
}
