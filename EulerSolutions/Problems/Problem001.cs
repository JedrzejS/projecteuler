﻿using EulerSolutions.Common;

namespace EulerSolutions.Problems
{
    internal class Problem001 : IProblem
    {
        public string Key => "Problem001 - Multiples of 3 and 5";
        public string Result => SumMultiples().ToString();

        private int SumMultiples()
        {
            int sum = 0;
            for (int i = 0; i < 1000; i++)
                if (i % 3 == 0 || i % 5 == 0)
                    sum += i;
           
            return sum;
        }
    }
}
