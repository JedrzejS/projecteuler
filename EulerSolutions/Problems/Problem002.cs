﻿using EulerSolutions.Common;
using System.Collections.Generic;

namespace EulerSolutions.Problems
{
    internal class Problem002 : IProblem
    {
        public string Key => "Problem002 - Sum of even Fibonacci numbers";
        public string Result => SumEvenFibonacci().ToString();

        private int SumEvenFibonacci(int max = 4000000)
        {
            int sum = 0;
            var fibonacci = GenerateFibonacci(max);
            foreach (int n in fibonacci)
                if (n % 2 == 0) sum += n;
            return sum;
        }
        private List<int> GenerateFibonacci(int max)
        {
            var valList = new List<int>() { 1, 2 }; // starting point
            int iterator = 2;
            for (int i = 0; i < max; i++)
            {
                if (valList[iterator - 1] + valList[iterator - 2] < max)
                {
                    valList.Add(valList[iterator - 1] + valList[iterator - 2]);
                    iterator++;
                }
                else
                    break;
            }
            return valList;
        }
    }
}
