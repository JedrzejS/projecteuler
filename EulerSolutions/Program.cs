﻿using Spectre.Console;
using System;

namespace EulerSolutions
{
    class Program
    {
        static void Main(string[] args)
        {
            var mh = new MenuHelper();
            mh.HandleMenu();
        }
    }
}
