﻿using System.Collections.Generic;
using System.Linq;

namespace EulerSolutions.Common
{
    public static class Extensions
    {
        /// <summary>
        /// Splits an array into several smaller arrays.
        /// </summary>
        /// <typeparam name="T">The type of the array.</typeparam>
        /// <param name="array">The array to split.</param>
        /// <param name="size">The size of the smaller arrays.</param>
        /// <returns>An array containing smaller arrays.</returns>
        public static List<List<T>> Split<T>(this T[] array, int size)
        {
            List<List<T>> retVal = new();
            for (var i = 0; i < (float)array.Length / size; i++)
            {
                //yield return array.Skip(i * size).Take(size);
                retVal.Add(array.Skip(i * size).Take(size).ToList());
            }
            return retVal;
        }
        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
        {
            return new List<TSource>(source);
        }
    }
}
