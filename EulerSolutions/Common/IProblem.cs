﻿namespace EulerSolutions.Common
{
    public interface IProblem
    {
        string Key { get; }
        string Result { get; }
    }
}
