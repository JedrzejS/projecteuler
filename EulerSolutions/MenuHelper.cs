﻿using Spectre.Console;
using System.Linq;

namespace EulerSolutions
{
    internal class MenuHelper
    {
        public void HandleMenu()
        {
            var ph = new ProblemHelper();
            ph.RegisterProblems();

            var availableProblems = ph._availableProblems;
            var problemKeys = availableProblems.Select(x => x.Key).ToArray();


            bool exit = false;
            while (!exit)
            {
                AnsiConsole.Write(
                new FigletText("Euler Project")
                    .Centered()
                    .Color(Color.Green));
                
                var selection = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .PageSize(3)
                    .AddChoices(new[] { "Show problems", "Exit" }));

                switch (selection)
                {
                    case "Show problems":
                        var selectedKey = OpenProblemMenu(problemKeys);
                        var result = availableProblems.FirstOrDefault(x => x.Key.Equals(selectedKey)).Result;
                        ShowResult(result, ref exit);
                        break;
                    case "Exit":
                        exit = true;
                        break;
                }
            }
        }
        private string OpenProblemMenu(string[] problemKeys)
        {
            AnsiConsole.Clear();

            AnsiConsole.Write(
                new FigletText("Euler Project")
                    .Centered()
                    .Color(Color.Green));

            var problemKey = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title("Please select a problem to run.")
                    .PageSize(10)
                    .MoreChoicesText("[grey](Move up and down to reveal more problems)[/]")
                    .AddChoices(problemKeys));
            return problemKey;
        }
        private void ShowResult(string result, ref bool exit)
        {
            AnsiConsole.Clear();
            AnsiConsole.Write(
                new FigletText("Euler Project")
                    .Centered()
                    .Color(Color.Green));

            AnsiConsole.MarkupLine($"The result is: [green]{result}[/]");
            
            var selectedOption = AnsiConsole.Prompt(
                new SelectionPrompt<string>()
                    .Title("")
                    .PageSize(3)
                    .AddChoices(new[] { "Go back", "Exit" }));

            if (selectedOption.Equals("Exit"))
                exit = true;
            else
                AnsiConsole.Clear();
        }
    }
}
