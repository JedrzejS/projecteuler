﻿using EulerSolutions.Common;
using EulerSolutions.Problems;
using System.Collections.Generic;

namespace EulerSolutions
{
    internal class ProblemHelper
    {
        public IList<IProblem> _availableProblems;
        public void RegisterProblems()
        {
            _availableProblems = new List<IProblem>
            {
                { new Problem001() },
                { new Problem002() },
                { new Problem003() },
                { new Problem011() },
                { new Problem012() }
            };
        }
    }
}
