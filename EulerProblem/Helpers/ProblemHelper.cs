﻿using EulerProblem.Common;
using EulerProblem.Problems;
using System.Collections.Generic;

namespace EulerProblem.Helpers
{
    internal class ProblemHelper
    {
        public IList<IProblem> _availableProblems;
        public void RegisterProblems()
        {
            _availableProblems = new List<IProblem>
            {
                { new Problem001() },
                { new Problem002() },
                { new Problem003() },
                { new Problem011() },
                { new Problem012() }
            };
        }
    }
}
