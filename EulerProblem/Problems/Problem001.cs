﻿using EulerProblem.Common;

namespace EulerProblem.Problems
{
    internal class Problem001 : IProblem
    {
        public string Key => "001";
        public string ShortDesc => "Problem001 - Multiples of 3 and 5";
        public string Desc => "";
        public string Result => SumMultiples().ToString();

        private int SumMultiples()
        {
            int sum = 0;
            for (int i = 0; i < 1000; i++)
                if (i % 3 == 0 || i % 5 == 0)
                    sum += i;

            return sum;
        }
    }
}
