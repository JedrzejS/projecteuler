﻿using EulerProblem.Common;
using System.Collections.Generic;

namespace EulerProblem.Problems
{
    internal class Problem012 : IProblem
    {
        public string Key => "012";
        public string ShortDesc => "Problem012 - Highly divisible triangular number";
        public string Desc => "";
        public string Result => FindTriangleNumber().ToString();

        private int FindTriangleNumber()
        {
            var triangleNumbers = GenerateTriangleNumbers(10);
            int devNo = 0;
            foreach (var trNo in triangleNumbers)
            {

            }
            return 0;
        }

        private List<int> GenerateTriangleNumbers(int no)
        {
            var retList = new List<int>() { 1 };
            for (int i = 2; i <= no; i++)
            {
                retList.Add(retList[retList.Count - 1] + i);
            }
            return retList;
        }
    }
}
