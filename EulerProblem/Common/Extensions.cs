﻿using System.Collections.Generic;
using System.Linq;

namespace EulerProblem.Common
{
    public static class Extensions
    {
        public static List<List<T>> Split<T>(this T[] array, int size) // taken from some stackoverflow answer...
        {
            List<List<T>> retVal = new();
            for (var i = 0; i < (float)array.Length / size; i++)
                retVal.Add(array.Skip(i * size).Take(size).ToList());
            
            return retVal;
        }
        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
        {
            return new List<TSource>(source);
        }
    }
}
