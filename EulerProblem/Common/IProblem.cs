﻿namespace EulerProblem.Common
{
    internal interface IProblem
    {
        string Key { get; }
        string ShortDesc { get; }
        string Desc { get; }
        string Result { get; }
    }
}
